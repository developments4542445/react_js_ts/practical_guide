# React and TypeScript - The Practical Guide

It is a course for React and TypeScript in udemy

#### Resources:
https://github.com/academind/react-typescript-course-resources
React course: https://www.youtube.com/watch?v=4baq00tHfmA&ab_channel=Academind

### How to compile TypeScript (and generate a new JavaScript file):
npx tsc <FILENAME>.ts

### How to create a new project:
npm create vite@latest <PROJECT_NAME>
cd <PROJECT_NAME>

#### Start server:
npm install
npm run dev

### Installing redux:
npm install @reduxjs/toolkit react-redux
