import { type FC, type PropsWithChildren } from 'react';

/*type CourseGoalProps = {
    title: string; 
    description: string; 
}*/

type CourseGoalProps = PropsWithChildren<{ 
    id: number;
    title: string;
    description: string;
    onDelete: (id: number) => void; 
}>;

export default function CourseGoal({ id, title, description, onDelete }: CourseGoalProps) {
    return (
        <article>
            <div>
                <h2>{title}</h2>
                <p>{description}</p>
            </div>
            <button onClick= {() => onDelete(id) }>Delete</button>
        </article>
    );
}

/*export const CourseGoal: FC<CourseGoalProps> = ({ title, description, children}) => {
    return (
        <article>
            <div>
                <h2>{title}</h2>
                <p>{description}</p>
                <p>Other</p>
                {children}
            </div>
            <button>Delete</button>
        </article>
    );
};*/
