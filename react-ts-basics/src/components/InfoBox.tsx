import { type ReactNode } from 'react';

type HintBoxProps = {
    mode: 'hint';
    children: ReactNode;
};

type WarningBoxProps = {
    mode: 'warning';
    severity: 'low' | 'medium' | 'high';
    children: ReactNode;
};

type InfoBoxProps = HintBoxProps | WarningBoxProps;

/*type InfoBoxProps = {
    mode: 'hint' | 'warning';
    //severity?: 'low' | 'medium' | 'high';       // ? indicates that this property is optional
    severity: 'low' | 'medium' | 'high';
    children: ReactNode;
};*/

//Should include info and warnings
export default function InfoBox(props: InfoBoxProps) {
    const { children, mode } = props;

    if (mode === 'hint'){
        return (
            <aside className="infobox infobox-hint">
                <p>{ children }</p>
            </aside>
        );
    } else {
        const { severity } = props;
        
        return (
            <aside className={ `infobox infobox-warning warning--${ severity }` }>
                <h2>Warning</h2>
                <p>{ children }</p>
            </aside>
        );
    }
}
