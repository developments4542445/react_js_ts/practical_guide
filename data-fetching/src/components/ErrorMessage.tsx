type ErrorMessageprops = {
    text:string;
};

export default function ErrorMessage({ text }: ErrorMessageprops) {
    return (
        <aside id="Error">
            <h1>An error occurred!</h1>
            <p>{ text }</p>
        </aside>
    );
}
