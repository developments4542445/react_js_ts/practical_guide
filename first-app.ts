let userName: string;

// This will be an error from TypeScript
//userName = 34;

// This is allowed
userName = "Max";

let userAge: number;
userAge = 34;

let isValid: boolean;
isValid = true;

// -----
// -----

// If I want a variable to be string and number...
let userID: string | number = 'abc1';
userID = 123;

// This is not allowed in this variable
//userID = true;


// -----
// -----

// Here we declare an object.
// But I am forced to include number OR string in variables
let user: object;
user = {
    name: "Max",
    age: 34,
    isAdmin: true,
    id: 'abc'
};

// To avoid this, first we declare types. Then, values
let user2: {
    name: string;
    age: number;
    isAdmin: boolean;
    id: string | number;
};

// -----
// -----

// Declaring arrays
let hobbies: Array<string>;
let hobbies2: string[]; // number[], boolean[], {name: string, age: number}[]

hobbies = ['Sports', 'Cooking', 'Reading'];
hobbies2 = ['Sports', 'Cooking', 'Reading'];

// This will fail
//hobbies = [1, 2, 3];

// -----
// -----

const API_KEY = 'example';
// This will fail
//API_KEY = "other";

// We can add type of variables to a function
// When a function does not return, we specify with void
function add(a: number, b: number): void {}

// When a function does return, we specify with its type. 
// We can ommit this, as we have the return.
function add2(a: number, b: number): number {return 2;}

// -----
// -----

// Indicating a function type0
function calculate(a: number, b: number, calcFn: (a: number, b: number) => number) {
    calcFn(a, b);
}

// We call the function passing 2 values and a pointer
calculate(2, 5, add2);

// -----
// -----

// Creating our own types of variables
type AddFn = (a: number, b: number) => number;
function calculate2(a: number, b: number, calcFn: AddFn) {
    calcFn(a, b);
}

// -----
// -----

// Defining objects with interfaces instead of type
interface Credentials {
    password: string;
    email: string;
}

let creds: Credentials;
creds = {
    password: 'abc',
    email: 'def'
};

// -----
// -----

// You can always use type
// interface can be used only in objects or functions
// interface can be used when creating classes. Forces you to add properties and methods

class AuthCredentials implements Credentials {
    password: string;
    email: string;
    other: number;
}

function login(credentials: Credentials){}

login(new AuthCredentials())

// -----
// -----

// Merging types
type Admin = {
    permissions: string[];
};

type AppUser = {
    userName: string;
};

type AppAdmin = Admin & AppUser;
let admin: AppAdmin;
admin = {
    permissions: ['login'],
    userName: 'user'
};

// Merging interfaces
interface Admin2 {
    permissions: string[];
};

interface AppUser2 {
    userName: string;
};

interface AppAdmin2 extends Admin2, AppUser2 {};

let admin2: AppAdmin2;
admin2 = {
    permissions: ['login'],
    userName: 'user'
};

// -----
// -----

let role: string; // can be 'admin', 'user', 'editor'
role = 'admin';
role = 'user';
role = 'editor';
role = 'other'; // This is allowed... but not desired

let role2: 'admin' | 'user' | 'editor';
role2 = 'admin';
role2 = 'user';
role2 = 'editor';
// Now this is not allowed
//role2 = 'other';

// -----
// -----

// Checking for specific values in a type
type Role = 'admin' | 'user' | 'editor';
function performAction(action: string | number, role: Role) {
    if (role === 'admin')
        console.log("Is an admin");
    if (typeof action === 'number')
        console.log("Is a number");
}

// -----
// -----

//Generic types
// Array<> is a generic type
let roles: Array<Role>;

// Including type placeholder because I don't know what is in there
type DataStorage<T> = {
    storage: T[];
    add: (data: T) => void; //(input) => output
};

const textStorage: DataStorage<string> = {
  storage: [],
  add(data) {
    this.storage.push(data);
  }
};

// Including function with 2 generic types
function merge<T, U>(a: T, b: U) {
    return {
        ...a,
        ...b
    };
}

const newUser = merge<{name: string}, {age: number}>(
    {name: 'Max'}, 
    {age: 1}

);
