var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var userName;
// This will be an error from TypeScript
//userName = 34;
// This is allowed
userName = "Max";
var userAge;
userAge = 34;
var isValid;
isValid = true;
// -----
// -----
// If I want a variable to be string and number...
var userID = 'abc1';
userID = 123;
// This is not allowed in this variable
//userID = true;
// -----
// -----
// Here we declare an object.
// But I am forced to include number OR string in variables
var user;
user = {
    name: "Max",
    age: 34,
    isAdmin: true,
    id: 'abc'
};
// To avoid this, first we declare types. Then, values
var user2;
// -----
// -----
// Declaring arrays
var hobbies;
var hobbies2; // number[], boolean[], {name: string, age: number}[]
hobbies = ['Sports', 'Cooking', 'Reading'];
hobbies2 = ['Sports', 'Cooking', 'Reading'];
// This will fail
//hobbies = [1, 2, 3];
// -----
// -----
var API_KEY = 'example';
// This will fail
//API_KEY = "other";
// We can add type of variables to a function
// When a function does not return, we specify with void
function add(a, b) { }
// When a function does return, we specify with its type. 
// We can ommit this, as we have the return.
function add2(a, b) { return 2; }
// -----
// -----
// Indicating a function type0
function calculate(a, b, calcFn) {
    calcFn(a, b);
}
// We call the function passing 2 values and a pointer
calculate(2, 5, add2);
function calculate2(a, b, calcFn) {
    calcFn(a, b);
}
var creds;
creds = {
    password: 'abc',
    email: 'def'
};
// -----
// -----
// You can always use type
// interface can be used only in objects or functions
// interface can be used when creating classes. Forces you to add properties and methods
var AuthCredentials = /** @class */ (function () {
    function AuthCredentials() {
    }
    return AuthCredentials;
}());
function login(credentials) { }
login(new AuthCredentials());
var admin;
admin = {
    permissions: ['login'],
    userName: 'user'
};
;
;
;
var admin2;
admin2 = {
    permissions: ['login'],
    userName: 'user'
};
// -----
// -----
var role; // can be 'admin', 'user', 'editor'
role = 'admin';
role = 'user';
role = 'editor';
role = 'other'; // This is allowed... but not desired
var role2;
role2 = 'admin';
role2 = 'user';
role2 = 'editor';
function performAction(action, role) {
    if (role === 'admin')
        console.log("Is an admin");
    if (typeof action === 'number')
        console.log("Is a number");
}
// -----
// -----
//Generic types
// Array<> is a generic type
var roles;
var textStorage = {
    storage: [],
    add: function (data) {
        this.storage.push(data);
    }
};
// Including function with 2 generic types
function merge(a, b) {
    return __assign(__assign({}, a), b);
}
var newUser = merge({ name: 'Max' }, { age: 1 });
